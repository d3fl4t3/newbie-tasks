s = input("Enter flag: ")

if s[:5] + s[-1] == "flag{}":
    print("[+] Flag format is correct")
else:
    print("[-] Flag format is incorrect")
    exit()

if s[-1::-2] == "}31sF_nk3t34uYgl" and s[-2::-2] == "sCl_0G1_H_r_0{af":
    print("[+] Flag correct!")
else:
    print("[-] Flag incorrect!")
    exit()

print("Task solved, your flag is %s" % s)
