pin = input("Enter pin code: ")

code = 11032930229521291733073397300432745129471297542241968824 

if len(pin) != 4:
    print("PIN should be 4 digits - check #1 fails")
    exit()

for c in pin:
    if not c.isdigit():
        print("PIN should be 4 digits - check #2 fails")
        exit()

import binascii
if binascii.crc32(pin.encode()) != 1745123251:
    print("PIN is incorrect - check #3 fails")
    exit()

flag = "flag{" + (int(pin*10)^code).to_bytes(32,"big").decode() + "}"
print(flag)

