import random

random.seed(12345)

first = int(input("Guess number from 0 to 5: "))

if random.randint(0,5) != first:
    print("No!")
    exit()

second = int(input("Guess number from 0 to 100: "))

if random.randint(0,100) != second:
    print("No!")
    exit()

third = int(input("Guess number from 0 to 1000000000: "))

if random.randint(0,1000000000) != third:
    print("No!")
    exit()

key = first*second*third

flag = -304641690299999989616213896040279989028959013841568599363475 + key*10**50

print("Flag is flag{%s}" % flag.to_bytes(64, "big").decode())
