key = input("Enter password: ")

if len(key) < 10000000000000000000000000000000000000000000000000:
    print("Your password is too short")
else:
    print("Great password, your flag:")
    print(bytearray.fromhex("666c61677b50347373573072445f31735f5573336c3353737d").decode())
