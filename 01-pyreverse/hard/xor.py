KEY = 666 ^ 1234 * 123**3

s = input("Enter flag: ")

def encrypt(text, password):
    return ''.join([chr(ord(a) ^ ord(b)) for a,b in zip(text, password*len(text))])

if encrypt(s, hex(KEY)[2:]).encode().hex()[::-1] == "5e5405021e120a0e5d4c0d0800123a13414c0c0a0b3e0d164a4c1718"[::-1]:
    print("Correct!")
else:
    print("Incorrect!")
